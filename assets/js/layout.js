var checkWhite;

$(() => {
    var logoSrc = $("#logo").attr('src');

    if (logoSrc.indexOf('white') > -1)
        checkWhite = true;
    else
        checkWhite = false;
});

var newLogo = '';

$(window).scroll(() => {
    var logo = '';

    if ($(this).scrollTop() > 0) {
        $('.fixed-bar').addClass('scrolled');
        $('.btn-up').addClass('d-block');

        logo = 'logo-black';
    }
    else {
        $('.fixed-bar').removeClass('scrolled');
        $('.btn-up').removeClass('d-block');

        logo = checkWhite ? 'logo-white' : 'logo-black';
    }

    if (newLogo != logo) {
        newLogo = logo;

        $("#logo").attr('src', `/assets/img/logo/${logo}.png`);
    }
});

function pageUp() {
    $("html, body").animate({ scrollTop: "0" }, 500);
    return false;
}

function splash() {
    $('#splashscreen').fadeOut(500);
}

function menuToggle() {
    $('#mobile-menu').toggleClass('opened');
}