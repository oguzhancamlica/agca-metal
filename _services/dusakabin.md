---
page: services
layout: service-detail
title: Duşakabin
header: Duşakabin
description: Duşakabin modelleri
styles:
    - name: services
data:
    imgFolder: dusakabin
    items:
      - id: 1
        img: dusakabin1.jpg
---