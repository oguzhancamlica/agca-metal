---
page: services
layout: service-detail
title: Katlanır Cam
header: Katlanır Cam
description: Katlanır cam modelleri
styles:
    - name: services
data:
    imgFolder: katlanir-cam
    items:
      - id: 1
        img: katlanir-cam1.jpg
---