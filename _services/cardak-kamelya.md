---
page: services
layout: service-detail
title: Çardak Kamelya
header: Çardak Kamelya
description: Çardak Kamelya modelleri
styles:
    - name: services
data:
    imgFolder: cardak-kamelya
    items:
      - id: 1
        img: cardak1.jpg
      - id: 2
        img: cardak2.jpg
      - id: 3
        img: cardak3.jpg
      - id: 4
        img: cardak4.jpg
      - id: 5
        img: cardak5.jpg
---