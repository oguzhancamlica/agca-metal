---
page: services
layout: service-detail
title: Tente
header: Tente
description: Tente modelleri
styles:
    - name: services
data:
    imgFolder: tente
    items:
      - id: 1
        img: tente1.jpg
      - id: 2
        img: tente2.jpg
      - id: 3
        img: tente3.jpeg
      - id: 4
        img: tente4.jpg
      - id: 5
        img: tente5.jpg
      - id: 6
        img: tente6.jpg
      - id: 7
        img: tente7.jpg
      - id: 8
        img: tente8.jpg
      - id: 9
        img: tente9.jpg
      - id: 10
        img: tente10.jpg
      - id: 11
        img: tente11.jpg
      - id: 12
        img: tente12.jpg
      - id: 13
        img: tente13.jpg
      - id: 14
        img: tente14.jpg
      - id: 15
        img: tente15.jpeg
      - id: 16
        img: tente16.jpeg
      - id: 17
        img: tente17.jpeg
      - id: 18
        img: tente18.jpeg
      - id: 19
        img: tente19.jpeg
      - id: 20
        img: tente20.jpeg
---