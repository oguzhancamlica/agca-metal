_data = null;
function imgShow(index) {
    index = parseInt(index) - 1;
    $(".img-gallery-container").addClass('show');
    $(".img-card").addClass('show');

    $("#detail-img").css('max-height', $(".img-gallery-container").innerHeight() - 120 + 'px');
    _data = { data: _services[index], index: index };
    setImg(_data.data.img);
}

function setImg(img) {
    $("#detail-img").fadeOut(0, function () {
        $("#img-text").text(_data.data.serviceName + " " + (_data.index + 1));
        $("#detail-img").attr('src', '/assets/img/' + img);
        $("#detail-img").attr('alt', _data.data.serviceName);
        $("#currentPage").text(_data.index + 1 + ' / ' + _services.length);
    }).fadeIn(300);
}

function imgError(){
    $("#detail-img").attr('src', '/assets/img/img-loading.jpg');
}

function next() {
    var nextItem = _services[_data.index + 1];
    if (nextItem) {
        _data.index++;
        setImg(nextItem.img);
    }
}

function prev() {
    var prevItem = _services[_data.index - 1];
    if (prevItem) {
        _data.index--;
        setImg(prevItem.img);
    }
}

$(window).keyup((ev) => {
    var key = ev.key;
    var keyCode = ev.keyCode;
    if (key === "ArrowRight" || keyCode === 39)
        next();
    else if (key === "ArrowLeft" || keyCode === 37)
        prev();
    else if (key === "Escape" || keyCode === 27)
        closeImg();
    return;
})

function closeImg() {
    $(".img-gallery-container").removeClass('show');
    $(".img-card").removeClass('show');
}

function outSideClick(e) {
    var container = $(".img-card");

    if (!container.is(e.target) && container.has(e.target).length === 0)
        closeImg();
}

_services = [];
function _setData(value) {
    _services.push(value);
}